## Description

Reads raw descriptors from any plugged-in USB-camera. Saves each configuration in its own HTML-file that can be opened and interacted with. See example HTML-file in **output** folder. 

Each descriptor can be clicked to open pop-up window with detailed information, this window can be dragged. Unrecognized descriptors appear as *--not implemented--*.

## Usage 

Just run executable, once finished it will prompt to press Enter. HTML-file(s) will appear in the same location. Open them in any browser, click on descriptor blocks.

## Requirements

- Windows operation system
- .NET Frameword 4.8 (though it can be rebuilt for a lesser version)

## Limitations

- USB-camera must be plugged into root hub, meaning into laptop/desktop directly and not into external hub.
- Not all possible descriptors are recognized, it only recognizes those that my cameras had. 
- It has very limited support for audio descriptors of USB Audio Class v1.0