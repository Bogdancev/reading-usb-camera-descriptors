﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

namespace ReadingUSBCameraDescriptors.USB
{
    public enum eClass { NA, CONFIG, VIDEO, AUDIO }
    public enum eSubClass { NA, VC, VS, AC, AS }
    //public enum eStage { Standard, VIDEO, VC, VS, AUDIO, NA }
    public class USBDevice
    {
        //-----------------------------------------------------------------------------------------
        #region Private properties & members

        const string mTemplateHTML = "ReadingUSBCameraDescriptors.USB.Config.html";
        const string mDescriptorsXML = "ReadingUSBCameraDescriptors.USB.Descriptors.xml";

        static XElement mXLookups = null;
        static IEnumerable<XElement> mXDescriptorTypes = null;

        string mHTML = null;
        string mContent = string.Empty;
        //eStage mStage = eStage.Standard; //Change with <section> creation
        eClass mClass = eClass.CONFIG;
        eSubClass mSubClass = eSubClass.NA;
        string mVersion = "0x0000";
        string mASFormatType = "";
        bool mIsSectionClosed = true;

        #endregion
        //-----------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------
        #region Public properties & methods

        public int ConfigNumber { get; set; }
        public byte[] ConfigDescriptors { get; set; }
        public string Product { get; set; }

        public void SaveHtml()
        {
            if (null == mXLookups) LoadDescriptorsXML();
            LoadHTML();
            mContent += $"<h3>{Product}</h3>\n";

            int Index = 0;
            do
            {
                byte DType = ConfigDescriptors[Index + 1];
                // Config/interface association/interface descriptors
                if (0x02 == DType || 0x0B == DType || (0x04 == DType && 0 == ConfigDescriptors[Index + 3]))
                {
                    if (!mIsSectionClosed) mContent += "</section>\n";
                    if (0x02 == DType || 0x0B == DType) mContent += "<section>\n";
                    else mContent += "<section class=\"background\">\n";
                    mIsSectionClosed = false;
                }
                if (0x04 == DType && 0 == ConfigDescriptors[Index + 3]) mContent += AddHeader(Index);
                if (0x0B == DType) SetStageAssociation(Index);
                mContent += ParseDescriptor(Index);
                if (0x04 == DType && 0 == ConfigDescriptors[Index + 3]) SetStageInterface(Index);
                Index += ConfigDescriptors[Index];
            }
            while (Index < ConfigDescriptors.Length);
            if (!mIsSectionClosed) mContent += "</section>\n";

            string Path = $"{Product} __{ConfigNumber}__.html";
            mHTML = mHTML.Replace("{INSERTION_POINT_TITLE}", Product);
            mHTML = mHTML.Replace("{INSERTION_POINT_BODY}", mContent);
            File.WriteAllText(Path, mHTML);
        }

        #endregion
        //-----------------------------------------------------------------------------------------

        void LoadDescriptorsXML()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            using (Stream s = assembly.GetManifestResourceStream(mDescriptorsXML))
            using (StreamReader reader = new StreamReader(s))
            {
                string text = reader.ReadToEnd();
                XDocument mDoc = XDocument.Parse(text);
                mXLookups = mDoc.Element("Structures").Element("Lookups");
                mXDescriptorTypes = mDoc.Element("Structures").Element("Descriptors").Descendants("Descriptor");
            }
        }

        void LoadHTML()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            using (Stream s = assembly.GetManifestResourceStream(mTemplateHTML))
            using (StreamReader reader = new StreamReader(s))
            {
                mHTML = reader.ReadToEnd();
            }
        }

        string ParseDescriptor(int Index)
        {
            string Fields = "", DescriptorName = "--Not Implemented--";

            XElement xDescriptor = SelectDescriptor(Index);
            if (null == xDescriptor)
            {
                string DescriptorType = "0x" + ConfigDescriptors[Index + 1].ToString("X2");
                string DescriptorSubType = "0x" + ConfigDescriptors[Index + 2].ToString("X2");
                Fields += $"      <b data-field=\"bDescriptorType\" data-value=\"{DescriptorType}\" " +
                    $"data-description=\"--Not Implemented ({DescriptorSubType})--\"></b>\n";
            }
            else
            {
                PopulateFields(Index, ref Fields, ref DescriptorName, xDescriptor);
            }

            return PopulateMarkup(ConfigDescriptors[Index], DescriptorName, Fields);
        }

        void SetStageAssociation(int Index)
        {
            byte Class = ConfigDescriptors[Index + 4];
            byte Subclass = ConfigDescriptors[Index + 5];

            if (0x01 == Class) { mClass = eClass.AUDIO; mSubClass = eSubClass.NA; return; }
            if (0x0E == Class) { mClass = eClass.VIDEO; mSubClass = eSubClass.NA; return; }

            mClass = eClass.NA;
            mSubClass = eSubClass.NA;
        }

        void SetStageInterface(int Index)
        {
            byte Subclass = ConfigDescriptors[Index + 6];

            if (eClass.VIDEO == mClass && 0x01 == Subclass) { mSubClass = eSubClass.VC; return; }
            if (eClass.VIDEO == mClass && 0x02 == Subclass) { mSubClass = eSubClass.VS; return; }
            if (eClass.AUDIO == mClass && 0x01 == Subclass) { mSubClass = eSubClass.AC; return; }
            if (eClass.AUDIO == mClass && 0x02 == Subclass) { mSubClass = eSubClass.AS; return; }

            mSubClass = eSubClass.NA;
        }

        string AddHeader(int Index)
        {
            byte Number = ConfigDescriptors[Index + 2];
            byte Class = ConfigDescriptors[Index + 5];
            byte SubClass = ConfigDescriptors[Index + 6];
            string Name = "--not implemented--";

            if (0x01 == Class && 0x01 == SubClass) Name = "Audio Control";
            if (0x01 == Class && 0x02 == SubClass) Name = "Audio Streaming";
            if (0x01 == Class && 0x03 == SubClass) Name = "MIDI Streaming";
            if (0x0E == Class && 0x01 == SubClass) Name = "Video Control";
            if (0x0E == Class && 0x02 == SubClass) Name = "Video Streaming";

            return $"<header>Interface {Number} ({Name})</header>";
        }

        XElement SelectDescriptor(int Index)
        {
            XElement xDescriptor = null;
            byte bDescriptorType = ConfigDescriptors[Index + 1];
            byte bDescriptorSubType = ConfigDescriptors[Index + 2];
            string strDescriptorType = "0x" + bDescriptorType.ToString("X2");
            if (0x24 == bDescriptorType)
            {
                if (1 == bDescriptorSubType && (eSubClass.VC == mSubClass || eSubClass.AC == mSubClass))
                    mVersion = "0x" + ConfigDescriptors[Index + 4].ToString("X2") + ConfigDescriptors[Index + 3].ToString("X2");

                string strDescriptorSubType = "0x" + ConfigDescriptors[Index + 2].ToString("X2");

                if (2 == bDescriptorSubType && eSubClass.AS == mSubClass)
                    xDescriptor = mXDescriptorTypes.SingleOrDefault(s
                        => s.Attribute("v")?.Value == strDescriptorType
                        && s.Attribute("s")?.Value == strDescriptorSubType
                        && s.Attribute("t")?.Value == mSubClass.ToString()
                        && s.Attribute("ft")?.Value == mASFormatType    //Audio streaming format type (1, 2, 3 or 4)
                        && (s.Attribute("ver")?.Value.IndexOf(mVersion) ?? -1) != -1
                        );
                else
                    xDescriptor = mXDescriptorTypes.SingleOrDefault(s
                        => s.Attribute("v")?.Value == strDescriptorType
                        && s.Attribute("s")?.Value == strDescriptorSubType
                        && s.Attribute("t")?.Value == mSubClass.ToString()
                        && (s.Attribute("ver")?.Value.IndexOf(mVersion) ?? -1) != -1
                        );
            }
            else if (0x25 == bDescriptorType)
            {
                xDescriptor = mXDescriptorTypes.SingleOrDefault(s
                    => s.Attribute("v")?.Value == strDescriptorType
                    && s.Attribute("t")?.Value == mClass.ToString()
                );
            }
            else
                xDescriptor = mXDescriptorTypes.SingleOrDefault(s => s.Attribute("v").Value == strDescriptorType);
            return xDescriptor;
        }

        void PopulateFields(int Index, ref string Fields, ref string DescriptorName, XElement xDescriptor)
        {
            Index++; //Skipping bLength (always 1 byte)
            int More = 0;
            bool IsMore = false;
            foreach (var xElem in xDescriptor.Descendants("f"))
            {
                string Description = "";
                string Field = xElem.Attribute("n").Value;
                string ValueSize = xElem.Attribute("s").Value;
                string ValueType = xElem.Attribute("t").Value;
                string Lookup = xElem.Attribute("l")?.Value;
                string LookupProperty = xElem.Attribute("lp")?.Value;
                string Rule = xElem.Attribute("r")?.Value;
                string NextRule = xElem.Attribute("nr")?.Value;
                string NextRuleField = xElem.Attribute("nrf")?.Value;
                string Array = xElem.Attribute("a")?.Value;
                string ArrayDescription = xElem.Attribute("ad")?.Value;
                string ArrayRule = xElem.Attribute("ar")?.Value;
                string ArrayType = xElem.Attribute("at")?.Value;
                string Check = xElem.Attribute("check")?.Value;

                string strValue = GetValue(ref Index, ValueSize, ValueType);
                if (null != Lookup)
                {
                    Description = GetFromLookup(strValue, Lookup, LookupProperty);
                    if ("bDescriptorType" == Field || "bDescriptorSubType" == Field) DescriptorName = Description;
                    if (!string.IsNullOrWhiteSpace(Check) && Check != Description)
                    {
                        More = Convert.ToInt32(xElem.Attribute("more")?.Value ?? "0") + 1;
                        IsMore = true;
                    }
                }
                else if (null != Rule)
                {
                    Description = xElem.Value ?? "";
                    ApplyRule(Rule, strValue, ref Description);
                }
                else
                {
                    Description = xElem.Value;
                    if (string.IsNullOrWhiteSpace(Description)) Description = "--Description not specified--";
                }
                if (IsMore) { if (0 >= More) continue; More--; }
                Fields += PopulateRow("      ", Field.ToString(), strValue, Description);
                if (null != Array) Fields += ProduceMultipleRows(ref Index, strValue, Array, ArrayDescription, ArrayRule, ArrayType);
                if (null != NextRule) Fields += ApplyNextRule(ref Index, NextRule, strValue, NextRuleField);
            }
        }

        string GetValue(ref int Index, string ValueSize, string ValueType)
        {
            if ("guid" == ValueType)
            {
                byte[] b = new byte[16];
                for (int i = 0; i < 16; i++)
                {
                    b[i] = ConfigDescriptors[Index];
                    Index++;
                }
                Guid guid = new Guid(b);
                return "{" + guid.ToString().ToUpper() + "}";
            }
            else
            {
                int Value = 0;
                int Size = Convert.ToInt32(ValueSize);
                if (4 < Size) throw new Exception("Value size is too big.");
                for (int i = 0; i < Size; i++)
                {
                    Value += ConfigDescriptors[Index] << (8 * i);
                    Index++;
                }
                switch (ValueType)
                {
                    case "hex": return "0x" + Value.ToString("X" + Size * 2); //0x01 or 0x00103
                    case "bin": return "0b" + Convert.ToString(Value, 2).PadLeft(8 * Size, '0');
                    default: return Value.ToString(); //dec
                }
            }
        }

        string ProduceMultipleRows(ref int Index, string strValue, string Array, string ArrayDescription, string Rule, string ArrayType)
        {
            //Assuming all values are decimals, because they are index numbers
            string Fields = "";
            int FieldsCount = Convert.ToInt32(strValue);
            if (string.IsNullOrWhiteSpace(ArrayType)) ArrayType = "dec";

            for (int i = 1; i <= FieldsCount; i++)
            {
                string V = GetValue(ref Index, "1", ArrayType);
                string Field = Array.Replace("(x)", $"({i})");

                if (!string.IsNullOrWhiteSpace(Rule))
                    ApplyRule(Rule, V, ref ArrayDescription);

                Fields += PopulateRow("      ", Field, V, ArrayDescription);
            }
            return Fields;
        }

        string GetFromLookup(string strValue, string Name, string LookupProperty)
        {
            if ("c" == LookupProperty) Name += $"_{mClass}";
            if ("sc" == LookupProperty) Name += $"_{mSubClass}";
            if ("csc" == LookupProperty) Name += $"_{mClass}_{mSubClass}";

            XElement xName = mXLookups.Element(Name)?.Descendants("n")
                .SingleOrDefault(s => s.Attribute("v").Value == strValue);

            if (null == xName)
            {
                mClass = eClass.NA;
                mSubClass = eSubClass.NA;
                return "--not implemented--";
            }
            else
            {
                if (eClass.NA == mClass)
                    return "--not implemented--";
                return xName.Value;
            }
        }

        string PopulateMarkup(byte Length, string DescriptorName, string Fields)
        {
            string Content = $"  <article>\n";
            Content += $"    <header>{DescriptorName}</header>\n";
            Content += "    <div class=\"hidden\">\n";
            Content += PopulateRow("      ", "bLength", Length.ToString(), "Size of this descriptor");
            Content += Fields;
            Content += "    </div>\n";
            Content += "  </article>\n";
            return Content;
        }

        string PopulateRow(string Padding, string Field, string Value, string Description)
        {
            return $"{Padding}<b data-field=\"{Field}\" data-value=\"{Value}\" data-description=\"{Description}\"></b>\n";
        }

        //-----------------------------------------------------------------------------------------
        #region Rules

        void ApplyRule(string Rule, string strValue, ref string Description)
        {
            switch (Rule)
            {
                case "ConfigAttributes": Description += ConfigAttributes(strValue); break;
                case "ConfigMaxPower": Description += ConfigMaxPower(strValue); break;
                case "EndpointProperties": Description += EndpointProperties(strValue); break;
                case "MaximumEndpointPacketSize": Description += MaximumEndpointPacketSize(strValue); break;
                case "StillImageEndpointProperties": Description += StillImageEndpointProperties(strValue); break;
                case "EnpointTransferType": Description += EnpointTransferType(strValue); break;
                case "EntpointPollingInterval": Description += EntpointPollingInterval(strValue); break;
                case "VSCapabilities": Description += VSCapabilities(strValue); break;
                case "VSStillCaptureMethod": Description += VSStillCaptureMethod(strValue); break;
                case "VSTriggerSupport": Description += VSTriggerSupport(strValue); break;
                case "VSTriggerUsage": Description += VSTriggerUsage(strValue); break;
                case "VSSpecificControls": Description += VSSpecificControls(strValue); break;
                case "CompressionFormats": Description += CompressionFormats(strValue); break;
                case "VSCopyProtect": Description += VSCopyProtect(strValue); break;
                case "VSInterlaceFlags": Description += VSInterlaceFlags(strValue); break;
                case "VSFrameCapabilities": Description += VSFrameCapabilities(strValue); break;
                case "FrameInterval": Description += FrameInterval(strValue); break;
                case "ColorPrimaries": Description += ColorPrimaries(strValue); break;
                case "TransferCharacteristics": Description += TransferCharacteristics(strValue); break;
                case "MatrixCoefficients": Description += MatrixCoefficients(strValue); break;
                case "FlagsMJPEG": Description += FlagsMJPEG(strValue); break;
                case "AudioTerminalTypes": Description += AudioTerminalTypes(strValue); break;
                case "AudioSpatialLocation": Description += AudioSpatialLocation(strValue); break;
                case "AudioFormatDataTypes": Description += AudioFormatDataTypes(strValue); break;
                case "AudioFormatTypes": Description += AudioFormatTypes(strValue); break;
                case "AudioEPAttributes": Description += AudioEPAttributes(strValue); break;
                case "AudioLockDelayUnits": Description += AudioLockDelayUnits(strValue); break;
                default: Description = "--Rule not specified--"; break;
            }
        }

        string ConfigAttributes(string strValue)
        {
            //0b10000000 -- D7 must be 1, D6 - Self-powered, D5 - Remote wakeup
            string Power = strValue[3] == '0' ? "Bus-powered, " : "Self-powered, ";
            string Wakeup = strValue[4] == '0' ? "no remote wakeup." : "Remote wakeup.";
            return Power + Wakeup;
        }

        string ConfigMaxPower(string strValue)
        {
            int Value = Convert.ToInt32(strValue);
            Value *= 2;

            return $"Max power consumption: {Value} mA";
        }

        string EndpointProperties(string strValue)
        {
            //0b10000000 -- D7 - direction, D3..0 - endpoint number
            string Direction = strValue[2] == '0' ? "OUT, " : "IN, ";
            int Number = Convert.ToInt32(strValue.Substring(6), 2);
            return Direction + " #" + Number.ToString();
        }

        string MaximumEndpointPacketSize(string strValue)
        {
            //0b0000101100000000 -- D15...13 - zeroes, D12,D11 - additional transactions, D10...D0 - size
            string TransactionCount = strValue.Substring(5, 2);
            string TransactionText = "--not specified--";
            switch (TransactionCount)
            {
                case "00": TransactionText = " (1 transaction per microframe)"; break;
                case "01": TransactionText = " (2 transactions per microframe)"; break;
                case "10": TransactionText = " (3 transactions per microframe)"; break;
                case "11": TransactionText = " (reserved)"; break;
            }
            uint Size = Convert.ToUInt32(strValue.Substring(2), 2);
            Size &= 0x7FF;

            return $"{Size} {TransactionText}";
        }

        string StillImageEndpointProperties(string strValue)
        {
            if ("0b00000000" == strValue) return "0 as Method 2 is used.";
            return EndpointProperties(strValue);
        }

        string EnpointTransferType(string strValue)
        {
            //0b10000000 -- D5..4 - Usage tyep, D3..2 - synchronization type, D1..0 - Transfer type
            string UsageType = "";
            switch (strValue.Substring(4, 2))
            {
                case "00": UsageType = "Data endpoint, "; break;
                case "01": UsageType = "Feedback endpoint, "; break;
                case "10": UsageType = "Implicit feedback Data endpoint, "; break;
                case "11": UsageType = "Reserved, "; break;
            }

            string SynchronizationType = "";
            switch (strValue.Substring(6, 2))
            {
                case "00": SynchronizationType = "No Synchronization, "; break;
                case "01": SynchronizationType = "Asynchronous, "; break;
                case "10": SynchronizationType = "Adaptive, "; break;
                case "11": SynchronizationType = "Synchronous, "; break;
            }

            string TransferType = "";
            switch (strValue.Substring(8, 2))
            {
                case "00": TransferType = "Control"; break;
                case "01": TransferType = "Isochronous"; break;
                case "10": TransferType = "Bulk"; break;
                case "11": TransferType = "Interrupt"; break;
            }
            return UsageType + SynchronizationType + TransferType;
        }

        string EntpointPollingInterval(string strValue)
        {
            int Number = Convert.ToInt32(strValue);
            Number = (int)Math.Pow(2, (Number - 1));
            return Number.ToString();
        }

        string VSCapabilities(string strValue)
        {
            //0b10000000 -- D0: 1 - Dynamic Format Change supported
            return strValue[9] == '1' ? "Dynamic forman change supported" : "No dynamic forman change supported";
        }

        string VSStillCaptureMethod(string strValue) => $"Method {strValue}";

        string VSTriggerSupport(string strValue)
        {
            return "Hardware triggering is " + ("0" == strValue ? "not supported" : "supported");
        }

        string VSTriggerUsage(string strValue)
        {
            if ("0" == strValue) return "Hardware trigger should initiate a still image capture";
            if ("1" == strValue) return "Host driver will notify client application of button pressed/released events";
            return "--value not supported--";
        }

        string VSSpecificControls(string strValue)
        {
            int intValue = Convert.ToInt32(strValue.Substring(2), 2);
            string Description = "";
            int Mask = 1;
            for (int i = 0; i < 8; i++)
            {
                switch (i)
                {
                    case 0: Description += (0 < (Mask & intValue)) ? "wKeyFrameRate, " : ""; break;
                    case 1: Description += (0 < (Mask & intValue)) ? "wPFrameRate, " : ""; break;
                    case 2: Description += (0 < (Mask & intValue)) ? "wCompQuality, " : ""; break;
                    case 3: Description += (0 < (Mask & intValue)) ? "wCompWindowSize, " : ""; break;
                    case 4: Description += (0 < (Mask & intValue)) ? "Generate Key Frame, " : ""; break;
                    case 5: Description += (0 < (Mask & intValue)) ? "Update Frame Segment, " : ""; break;
                }
                Mask = Mask << 1;
            }

            Description = Description.TrimEnd(new char[] { ' ', ',' });
            if (string.IsNullOrWhiteSpace(Description)) Description = "No VideoStreaming specific controls are supported";
            else Description = "Supported: " + Description;
            return Description;
        }

        string CompressionFormats(string strValue)
        {
            string Description = "Format: ";
            switch (strValue)
            {
                case "{32595559-0000-0010-8000-00AA00389B71}": Description += "YUY2"; break;
                case "{3231564E-0000-0010-8000-00AA00389B71}": Description += "NV12"; break;
                case "{3032344D-0000-0010-8000-00AA00389B71}": Description += "M420"; break;
                case "{30323449-0000-0010-8000-00AA00389B71}": Description += "I420"; break;
                default: Description = "--format mismatch--"; break;
            }
            return Description;
        }

        string VSCopyProtect(string strValue)
        {
            if ("0" == strValue) return "No restrictions imposed on the duplication of this video stream";
            if ("1" == strValue) return "Restrict duplication";
            return "--value mismatch--";
        }

        //TODO: D7..D6 -- display mode only for 0x0100 
        string VSInterlaceFlags(string strValue)
        {
            //0b10000000
            if ("0b00000000" == strValue) return "Non-interlaced stream";

            string Description = "";
            if ('1' == strValue[9]) Description += "Interlaced stream or variable, ";
            if ('0' == strValue[8]) Description += "2 fields per frame, ";
            if ('1' == strValue[8]) Description += "1 field per frame, ";
            if ('1' == strValue[7]) Description += "Field 1 first, ";

            Description += "Field pattern: ";
            switch (strValue.Substring(4, 2))
            {
                case "00": Description += "field 1 only"; break;
                case "01": Description += "field 2 only"; break;
                case "10": Description += "regular pattern of fields 1 and 2"; break;
                case "11": Description += "random pattern of fields 1 and 2"; break;
            }
            return Description;
        }

        string VSFrameCapabilities(string strValue)
        {
            string Description = "";
            if ('1' == strValue[9]) Description += "Still images using capture method 1 are supported, ";
            if ('1' == strValue[8]) Description += "Fixed frame rate, ";
            Description = Description.TrimEnd(new char[] { ' ', ',' });
            if (string.IsNullOrWhiteSpace(Description)) Description = "None";
            return Description;
        }

        string FrameInterval(string strValue)
        {
            try
            {
                double v = Convert.ToInt32(strValue);
                v = Math.Round(1 / (v / 10000000), 1, MidpointRounding.AwayFromZero);
                return $" ({v}fps)";
            }
            catch
            {
                return "";
            }

        }

        string ColorPrimaries(string strValue)
        {
            string Description = "";
            switch (strValue)
            {
                case "0": Description = "Unspecified (Image characteristics unknown)"; break;
                case "1": Description = "BT.709, sRGB (default)"; break;
                case "2": Description = "BT.470-2 (M)"; break;
                case "3": Description = "BT.470-2 (B, G)"; break;
                case "4": Description = "SMPTE 170M"; break;
                case "5": Description = "SMPTE 240M"; break;
                default: Description = "Reserved"; break;
            }
            return Description;
        }

        string TransferCharacteristics(string strValue)
        {
            string Description = "";
            switch (strValue)
            {
                case "0": Description = "Unspecified (Image characteristics unknown)"; break;
                case "1": Description = "BT.709 (default)"; break;
                case "2": Description = "BT.470-2 M"; break;
                case "3": Description = "BT.470-2 B, G"; break;
                case "4": Description = "SMPTE 170M"; break;
                case "5": Description = "SMPTE 240M"; break;
                case "6": Description = "Linear (V = Lc)"; break;
                case "7": Description = "sRGB (very similar to BT.709)"; break;
                default: Description = "Reserved"; break;
            }
            return Description;
        }

        string MatrixCoefficients(string strValue)
        {
            string Description = "";
            switch (strValue)
            {
                case "0": Description = "Unspecified (Image characteristics unknown)"; break;
                case "1": Description = "BT. 709"; break;
                case "2": Description = "FCC"; break;
                case "3": Description = "BT.470-2 B, G"; break;
                case "4": Description = "SMPTE 170M (BT.601, default)"; break;
                case "5": Description = "SMPTE 240M"; break;
                default: Description = "Reserved"; break;
            }
            return Description;
        }

        string FlagsMJPEG(string strValue)
        {
            //0b00000001 -- D0-Fixed Size Samples: 0-No, 1-Yes
            return '0' == strValue[9] ? "Not fixed size samples" : "Fixed size samples";
        }

        string AudioTerminalTypes(string strValue)
        {
            string Description = "";
            switch (strValue)
            {
                case "0x0100": Description = "USB Undefined"; break;
                case "0x0101": Description = "USB streaming"; break;
                case "0x01FF": Description = "USB vendor specific"; break;
                case "0x0200": Description = "Input Undefined"; break;
                case "0x0201": Description = "Microphone"; break;
                case "0x0202": Description = "Desktop microphone"; break;
                case "0x0203": Description = "Personal microphone"; break;
                case "0x0204": Description = "Omni-directional microphone"; break;
                case "0x0205": Description = "Microphone array"; break;
                case "0x0206": Description = "Processing microphone array"; break;
                case "0x0300": Description = "Output Undefined"; break;
                case "0x0301": Description = "Speaker"; break;
                case "0x0302": Description = "Headphones"; break;
                case "0x0303": Description = "Head Mounted Display Audio"; break;
                case "0x0304": Description = "Desktop speaker"; break;
                case "0x0305": Description = "Room speaker"; break;
                case "0x0306": Description = "Communication speaker"; break;
                case "0x0307": Description = "Low frequency effects speaker"; break;
                default: Description = "--not found in the list of possible values--"; break;
            }
            return Description;
        }

        string AudioSpatialLocation(string strValue)
        {
            int intValue = Convert.ToInt32(strValue.Substring(2), 2);
            string Description = "";
            int Mask = 1;
            for (int i = 0; i < 12; i++)
            {
                switch (i)
                {
                    case 0: Description += (0 < (Mask & intValue)) ? "Left Front (L), " : ""; break;
                    case 1: Description += (0 < (Mask & intValue)) ? "Right Front (R), " : ""; break;
                    case 2: Description += (0 < (Mask & intValue)) ? "Center Front (C), " : ""; break;
                    case 3: Description += (0 < (Mask & intValue)) ? "Low Frequency Enhancement (LFE), " : ""; break;
                    case 4: Description += (0 < (Mask & intValue)) ? "Left Surround (Ls), " : ""; break;
                    case 5: Description += (0 < (Mask & intValue)) ? "Right Surround (Rs), " : ""; break;
                    case 6: Description += (0 < (Mask & intValue)) ? "Left of Center (Lc), " : ""; break;
                    case 7: Description += (0 < (Mask & intValue)) ? "Right of Center (Rc), " : ""; break;
                    case 8: Description += (0 < (Mask & intValue)) ? "Surround (S), " : ""; break;
                    case 9: Description += (0 < (Mask & intValue)) ? "Side Left (Sl), " : ""; break;
                    case 10: Description += (0 < (Mask & intValue)) ? "Side Right (Sr), " : ""; break;
                    case 11: Description += (0 < (Mask & intValue)) ? "Top (T), " : ""; break;
                }
                Mask = Mask << 1;
            }

            Description = Description.TrimEnd(new char[] { ' ', ',' });
            if (string.IsNullOrWhiteSpace(Description)) Description = "Non-predefined spatial positions";
            else Description = "Spatial locations: " + Description;
            return Description;
        }

        /// <summary>
        /// Sets mASFormatType variable
        /// </summary>
        string AudioFormatDataTypes(string strValue)
        {
            string Description = "";
            switch (strValue)
            {
                case "0x0000": Description = "TYPE_I_UNDEFINED"; mASFormatType = "1"; break;
                case "0x0001": Description = "PCM"; mASFormatType = "1"; break;
                case "0x0002": Description = "PCM8"; mASFormatType = "1"; break;
                case "0x0003": Description = "IEEE_FLOAT"; mASFormatType = "1"; break;
                case "0x0004": Description = "ALAW"; mASFormatType = "1"; break;
                case "0x0005": Description = "MULAW"; mASFormatType = "1"; break;
                case "0x1000": Description = "TYPE_II_UNDEFINED"; mASFormatType = "2"; break;
                case "0x1001": Description = "MPEG"; mASFormatType = "2"; break;
                case "0x1002": Description = "AC-3"; mASFormatType = "2"; break;
                case "0x2000": Description = "TYPE_III_UNDEFINED"; mASFormatType = "3"; break;
                case "0x2001": Description = "IEC1937_AC-3"; mASFormatType = "3"; break;
                case "0x2002": Description = "IEC1937_MPEG-1_Layer1"; mASFormatType = "3"; break;
                case "0x2003": Description = "IEC1937_MPEG-1_Layer2/3 or IEC1937_MPEG-2_NOEXT"; mASFormatType = "3"; break;
                case "0x2004": Description = "IEC1937_MPEG-2_EXT"; mASFormatType = "3"; break;
                case "0x2005": Description = "IEC1937_MPEG-2_Layer1_LS"; mASFormatType = "3"; break;
                case "0x2006": Description = "IEC1937_MPEG-2_Layer2/3_LS"; mASFormatType = "3"; break;
                default: Description = "--not found in the list of possible values--"; mASFormatType = ""; break;
            }
            return Description;
        }

        string AudioFormatTypes(string strValue)
        {
            string Description = "";
            switch (strValue)
            {
                case "0x00": Description = "FORMAT_TYPE_UNDEFINED"; break;
                case "0x01": Description = "FORMAT_TYPE_I"; break;
                case "0x02": Description = "FORMAT_TYPE_II"; break;
                case "0x03": Description = "FORMAT_TYPE_II"; break;
                default: Description = "--not found in the list of possible values--"; break;
            }
            return Description;
        }

        string AudioEPAttributes(string strValue)
        {
            //0b10000000
            if ("0b00000000" == strValue) return "No controls supported";

            string Description = "Controls: ";
            if ('1' == strValue[9]) Description += "Sampling Frequency, ";
            if ('1' == strValue[8]) Description += "Pitch, ";
            if ('1' == strValue[7]) Description += "Max Packets Only, ";
            Description = Description.TrimEnd(new char[] { ' ', ',' });
            return Description;
        }

        string AudioLockDelayUnits(string strValue)
        {
            int intValue = Convert.ToInt32(strValue);
            switch(intValue)
            {
                case 0: return "Undefined";
                case 1: return "Milliseconds";
                case 2: return "Decoded PCM samples";
                default: return "Reserved";
            }
        }

        //---------------------------------------

        string ApplyNextRule(ref int Index, string Rule, string strValue, string NextRuleField)
        {
            switch (Rule)
            {
                case "ProcessingUnitControlls":
                    return ProcessingUnitControlls(ref Index, Rule, strValue, NextRuleField);
                case "CameraTerminalControlls":
                    return CameraTerminalControlls(ref Index, Rule, strValue, NextRuleField);
                case "ExtensionUnitControlls":
                    return ExtensionUnitControlls(ref Index, Rule, strValue, NextRuleField);
                case "StillImageWidthHeight":
                    return StillImageWidthHeight(ref Index, Rule, strValue);
                case "StillImageCompression":
                    return StillImageCompression(ref Index, Rule, strValue);
                case "FrameIntervalProgramming":
                    return FrameIntervalProgramming(ref Index, Rule, strValue);
                case "ASSupportedSamplingFrequencies":
                    return ASSupportedSamplingFrequencies(ref Index, Rule, strValue);
            }
            return "";
        }

        string ProcessingUnitControlls(ref int Index, string Rule, string strValue, string NextRuleField)
        {
            //strValue here is size of the next field
            string bmValue = GetValue(ref Index, strValue, "bin");
            int intValue = Convert.ToInt32(bmValue.Substring(2), 2);
            string Description = "";
            int NextFieldSizeBits = Convert.ToInt32(strValue) * 8;
            int Mask = 1;
            for (int i = 0; i < NextFieldSizeBits; i++)
            {
                switch (i)
                {
                    case 0: Description += (0 < (Mask & intValue)) ? "Brightness, " : ""; break;
                    case 1: Description += (0 < (Mask & intValue)) ? "Contrast, " : ""; break;
                    case 2: Description += (0 < (Mask & intValue)) ? "Hue, " : ""; break;
                    case 3: Description += (0 < (Mask & intValue)) ? "Saturation, " : ""; break;
                    case 4: Description += (0 < (Mask & intValue)) ? "Sharpness, " : ""; break;
                    case 5: Description += (0 < (Mask & intValue)) ? "Gamma, " : ""; break;
                    case 6: Description += (0 < (Mask & intValue)) ? "White Balance Temperature, " : ""; break;
                    case 7: Description += (0 < (Mask & intValue)) ? "White Balance Component, " : ""; break;
                    case 8: Description += (0 < (Mask & intValue)) ? "Backlight Compensation, " : ""; break;
                    case 9: Description += (0 < (Mask & intValue)) ? "Gain, " : ""; break;
                    case 10: Description += (0 < (Mask & intValue)) ? "Power Line Frequency, " : ""; break;
                    case 11: Description += (0 < (Mask & intValue)) ? "Hue (Auto), " : ""; break;
                    case 12: Description += (0 < (Mask & intValue)) ? "White Balance Temperature (Auto), " : ""; break;
                    case 13: Description += (0 < (Mask & intValue)) ? "White Balance Component (Auto), " : ""; break;
                    case 14: Description += (0 < (Mask & intValue)) ? "Digital Multiplier, " : ""; break;
                    case 15: Description += (0 < (Mask & intValue)) ? "Digital Multiplier Limit, " : ""; break;
                    case 16: Description += (0 < (Mask & intValue)) ? "Analog Video Standard, " : ""; break;
                    case 17: Description += (0 < (Mask & intValue)) ? "Analog Video Lock Status, " : ""; break;
                    case 18: Description += (0 < (Mask & intValue)) ? "Contrast (Auto), " : ""; break;
                }
                Mask = Mask << 1;
            }
            Description = Description.TrimEnd(new char[] { ' ', ',' });
            if (string.IsNullOrWhiteSpace(Description)) Description = "None";
            return PopulateRow("      ", NextRuleField, bmValue, Description);
        }

        string CameraTerminalControlls(ref int Index, string Rule, string strValue, string NextRuleField)
        {
            //strValue here is size of the next field
            string bmValue = GetValue(ref Index, strValue, "bin");
            int intValue = Convert.ToInt32(bmValue.Substring(2), 2);
            string Description = "";
            int NextFieldSizeBits = Convert.ToInt32(strValue) * 8;
            int Mask = 1;
            for (int i = 0; i < NextFieldSizeBits; i++)
            {
                switch (i)
                {
                    case 0: Description += (0 < (Mask & intValue)) ? "Scanning Mode, " : ""; break;
                    case 1: Description += (0 < (Mask & intValue)) ? "Auto-Exposure Mode, " : ""; break;
                    case 2: Description += (0 < (Mask & intValue)) ? "Auto-Exposure Priority, " : ""; break;
                    case 3: Description += (0 < (Mask & intValue)) ? "Exposure Time (Absolute), " : ""; break;
                    case 4: Description += (0 < (Mask & intValue)) ? "Exposure Time (Relative), " : ""; break;
                    case 5: Description += (0 < (Mask & intValue)) ? "Focus (Absolute), " : ""; break;
                    case 6: Description += (0 < (Mask & intValue)) ? "Focus (Relative), " : ""; break;
                    case 7: Description += (0 < (Mask & intValue)) ? "Iris (Absolute), " : ""; break;
                    case 8: Description += (0 < (Mask & intValue)) ? "Iris (Relative), " : ""; break;
                    case 9: Description += (0 < (Mask & intValue)) ? "Zoom (Absolute), " : ""; break;
                    case 10: Description += (0 < (Mask & intValue)) ? "Zoom (Relative), " : ""; break;
                    case 11: Description += (0 < (Mask & intValue)) ? "PanTilt (Absolute), " : ""; break;
                    case 12: Description += (0 < (Mask & intValue)) ? "PanTilt (Relative), " : ""; break;
                    case 13: Description += (0 < (Mask & intValue)) ? "Roll (Absolute), " : ""; break;
                    case 14: Description += (0 < (Mask & intValue)) ? "Roll (Relative), " : ""; break;
                    case 15: Description += (0 < (Mask & intValue)) ? "Reserved, " : ""; break;
                    case 16: Description += (0 < (Mask & intValue)) ? "Reserved, " : ""; break;
                    case 17: Description += (0 < (Mask & intValue)) ? "Focus, Auto, " : ""; break;
                    case 18: Description += (0 < (Mask & intValue)) ? "Privacy, " : ""; break;
                    case 19: Description += (0 < (Mask & intValue)) ? "Focus (Simple), " : ""; break;
                    case 20: Description += (0 < (Mask & intValue)) ? "Window, " : ""; break;
                    case 21: Description += (0 < (Mask & intValue)) ? "Region of Interest, " : ""; break;
                }
                Mask = Mask << 1;
            }
            Description = Description.TrimEnd(new char[] { ' ', ',' });
            if (string.IsNullOrWhiteSpace(Description)) Description = "None";
            return PopulateRow("      ", NextRuleField, bmValue, Description);
        }

        string ExtensionUnitControlls(ref int Index, string Rule, string strValue, string NextRuleField)
        {
            //strValue here is size of the next field
            string bmValue = GetValue(ref Index, strValue, "bin");
            int intValue = Convert.ToInt32(bmValue.Substring(2), 2);
            string Description = "Vendor-specific: 1 means that the mentioned control is supported";

            return PopulateRow("      ", NextRuleField, bmValue, Description);
        }

        string StillImageWidthHeight(ref int Index, string Rule, string strValue)
        {
            string Fields = "";
            int NextFields = Convert.ToInt32(strValue);
            for (int i = 1; i <= NextFields; i++)
            {
                string strW = GetValue(ref Index, "2", "dec");
                Fields += PopulateRow("      ", $"wWidth({i})", strW, $"Width of the still image in pattern {i}");
                string strH = GetValue(ref Index, "2", "dec");
                Fields += PopulateRow("      ", $"wHeight({i})", strH, $"Height of the still image in pattern {i}");
            }
            return Fields;
        }

        string StillImageCompression(ref int Index, string Rule, string strValue)
        {
            string Fields = "";
            int NextFields = Convert.ToInt32(strValue);
            for (int i = 1; i <= NextFields; i++)
            {
                string strPattern = GetValue(ref Index, "1", "dec");
                Fields += PopulateRow("      ", $"bCompression({i})", strPattern, $"Compression of the still image in pattern {i}");
            }
            return Fields;
        }

        string FrameIntervalProgramming(ref int Index, string Rule, string strValue)
        {
            string Fields = "--not specified---";
            try
            {

                int Count = Convert.ToInt32(strValue);
                if (0 == Count) //Continious frame interval
                {
                    string strFrameInterval = GetValue(ref Index, "4", "dec");
                    double v = Convert.ToInt32(strFrameInterval);
                    v = Math.Round(1 / (v / 10000000), 1, MidpointRounding.AwayFromZero);
                    Fields += PopulateRow("      ", $"dwMinFrameInterval", strFrameInterval, $"Shortest frame interval supported: {v}fps");

                    strFrameInterval = GetValue(ref Index, "4", "dec");
                    v = Convert.ToInt32(strFrameInterval);
                    v = Math.Round(1 / (v / 10000000), 1, MidpointRounding.AwayFromZero);
                    Fields += PopulateRow("      ", $"dwMaxFrameInterval", strFrameInterval, $"Longest frame interval supported: {v}fps");

                    strFrameInterval = GetValue(ref Index, "4", "dec");
                    v = Convert.ToInt32(strFrameInterval);
                    v = Math.Round(1 / (v / 10000000), 1, MidpointRounding.AwayFromZero);
                    Fields += PopulateRow("      ", $"dwFrameIntervalStep", strFrameInterval, $"Granularity of frame interval range: {v}fps");
                }
                else //strValue indicates number of frame intervals
                {
                    for (int i = 1; i <= Count; i++)
                    {
                        string strFrameInterval = GetValue(ref Index, "4", "dec");
                        double v = Convert.ToInt32(strFrameInterval);
                        v = Math.Round(1 / (v / 10000000), 1, MidpointRounding.AwayFromZero);
                        Fields += PopulateRow("      ", $"dwFrameInterval({i})", strFrameInterval, $"{v}fps");
                    }
                }
            }
            catch { }
            return Fields;
        }

        string ASSupportedSamplingFrequencies(ref int Index, string Rule, string strValue)
        {
            string Fields = "--not specified---";
            try
            {

                int Count = Convert.ToInt32(strValue);
                if (0 == Count) //Continuous sampling frequency
                {
                    string strFrameInterval = GetValue(ref Index, "3", "dec");
                    double v = Convert.ToInt32(strFrameInterval);
                    Fields += PopulateRow("      ", $"tLowerSamFreq", strFrameInterval, $"Lower bound in Hz of the sampling frequency range for this isochronous data endpoint");

                    strFrameInterval = GetValue(ref Index, "3", "dec");
                    v = Convert.ToInt32(strFrameInterval);
                    Fields += PopulateRow("      ", $"tUpperSamFreq", strFrameInterval, $"Upper bound in Hz of the sampling frequency range for this isochronous data endpoint");
                }
                else //strValue indicates number of frame intervals
                {
                    for (int i = 1; i <= Count; i++)
                    {
                        string strFrameInterval = GetValue(ref Index, "3", "dec");
                        double v = Convert.ToInt32(strFrameInterval);
                        Fields += PopulateRow("      ", $"tSamFreq[{i}]", strFrameInterval, $"Sampling frequency {i} in Hz for this isochronous data endpoint");
                    }
                }
            }
            catch { }
            return Fields;
        }

        #endregion
        //-----------------------------------------------------------------------------------------
    }
}
