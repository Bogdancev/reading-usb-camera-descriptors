﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace ReadingUSBCameraDescriptors.USB
{
    public partial class USBHost
    {
        const int GENERIC_WRITE = 0x40000000;
        const int FILE_SHARE_WRITE = 0x2;
        const int OPEN_EXISTING = 0x3;
        const int INVALID_HANDLE_VALUE = -1;

        const int IOCTL_USB_GET_ROOT_HUB_NAME = 0x220408;
        const int IOCTL_USB_GET_NODE_INFORMATION = 0x220408;
        const int IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX = 0x220448;
        const int IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION = 0x220410;

        const int BUFFER_SIZE = 2048;

        const byte USB_CONFIGURATION_DESCRIPTOR_TYPE = 0x02;
        const byte USB_STRING_DESCRIPTOR_TYPE = 0x03;

        const string GUID_DEVINTERFACE_HUBCONTROLLER = "3abf6f2d-71c4-462a-8a92-1e6861e6af27";
        const int DIGCF_PRESENT = 0x2;
        const int DIGCF_DEVICEINTERFACE = 0x10;

        List<string> mControllerPaths = new List<string>();
        List<(string, int)> mHubPaths = new List<(string Path, int PortCount)>();
        List<(string, List<(int, USB_DEVICE_DESCRIPTOR)>)> mPorts =
            new List<(string Path, List<(int PortNumber, USB_DEVICE_DESCRIPTOR Descriptor)>)>();

        public IEnumerable<USBDevice> GetVideoDevices()
        {
            List<USBDevice> Devices = new List<USBDevice>();
            mControllerPaths = GetControllerPaths();
            mHubPaths = GetHubPaths(mControllerPaths);
            mPorts = GetPorts(mHubPaths);
            Devices.AddRange(GetVideoDevices(mPorts));
            return Devices;
        }

        List<string> GetControllerPaths()
        {
            List<string> Paths = new List<string>();
            Guid HostGUID = new Guid(GUID_DEVINTERFACE_HUBCONTROLLER);

            IntPtr h = SetupDiGetClassDevs(ref HostGUID, 0, IntPtr.Zero, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
            if (INVALID_HANDLE_VALUE != h.ToInt64())
            {
                IntPtr ptrBuf = Marshal.AllocHGlobal(BUFFER_SIZE);
                bool Success;
                int InterfaceIndex = 0;
                do
                {
                    // Create a Device Interface Data structure
                    SP_DEVICE_INTERFACE_DATA dia = new SP_DEVICE_INTERFACE_DATA();
                    dia.cbSize = Marshal.SizeOf(dia);

                    // start the enumeration 
                    Success = SetupDiEnumDeviceInterfaces(h, IntPtr.Zero, ref HostGUID, InterfaceIndex, ref dia);
                    if (Success)
                    {
                        // build a DevInfo Data structure
                        SP_DEVINFO_DATA da = new SP_DEVINFO_DATA();
                        da.cbSize = Marshal.SizeOf(da);

                        // build a Device Interface Detail Data structure
                        SP_DEVICE_INTERFACE_DETAIL_DATA didd = new SP_DEVICE_INTERFACE_DETAIL_DATA();
                        didd.cbSize = (IntPtr.Size == 4) ? (4 + Marshal.SystemDefaultCharSize) : 8; // trust me :)

                        // now we can get some more detailed information
                        int nRequiredSize = 0;
                        int nBytes = BUFFER_SIZE;
                        if (SetupDiGetDeviceInterfaceDetail(h, ref dia, ref didd, nBytes, ref nRequiredSize, ref da))
                        {
                            Paths.Add(didd.DevicePath);
                        }
                    }
                    InterfaceIndex++;
                }
                while (Success);
            }
            return Paths;
        }

        List<(string Path, int PortCount)> GetHubPaths(List<string> ControllerPaths)
        {
            List<(string Path, int PortCount)> Paths = new List<(string Path, int PortCount)>();
            foreach (string ControllerPath in ControllerPaths)
            {
                (string Path, int PortCount) Path = ("", 0);
                // Open a handle to the Host Controller
                IntPtr h = CreateFile(ControllerPath, GENERIC_WRITE, FILE_SHARE_WRITE, IntPtr.Zero, OPEN_EXISTING, 0, IntPtr.Zero);
                if (h.ToInt32() != INVALID_HANDLE_VALUE)
                {
                    USB_ROOT_HUB_NAME HubName = new USB_ROOT_HUB_NAME();
                    int nBytes = Marshal.SizeOf(HubName);
                    IntPtr ptrHubName = Marshal.AllocHGlobal(nBytes);

                    // get the Hub Name
                    if (DeviceIoControl(h, IOCTL_USB_GET_ROOT_HUB_NAME, ptrHubName, nBytes, ptrHubName, nBytes, out int nBytesReturned, IntPtr.Zero))
                    {
                        HubName = (USB_ROOT_HUB_NAME)Marshal.PtrToStructure(ptrHubName, typeof(USB_ROOT_HUB_NAME));
                        Path.Path = @"\\.\" + HubName.RootHubName;
                    }

                    // Now let's open the Hub (based upon the HubName we got above)
                    IntPtr h2 = CreateFile(Path.Path, GENERIC_WRITE, FILE_SHARE_WRITE, IntPtr.Zero, OPEN_EXISTING, 0, IntPtr.Zero);
                    if (INVALID_HANDLE_VALUE != h2.ToInt64())
                    {
                        USB_NODE_INFORMATION NodeInfo = new USB_NODE_INFORMATION();
                        NodeInfo.NodeType = (int)USB_HUB_NODE.UsbHub;
                        nBytes = Marshal.SizeOf(NodeInfo);
                        IntPtr ptrNodeInfo = Marshal.AllocHGlobal(nBytes);
                        Marshal.StructureToPtr(NodeInfo, ptrNodeInfo, true);

                        // get the Hub Information
                        if (DeviceIoControl(h2, IOCTL_USB_GET_NODE_INFORMATION, ptrNodeInfo, nBytes, ptrNodeInfo, nBytes, out nBytesReturned, IntPtr.Zero))
                        {
                            NodeInfo = (USB_NODE_INFORMATION)Marshal.PtrToStructure(ptrNodeInfo, typeof(USB_NODE_INFORMATION));
                            Path.PortCount = NodeInfo.HubInformation.HubDescriptor.bNumberOfPorts;
                        }
                        Marshal.FreeHGlobal(ptrNodeInfo);
                        CloseHandle(h2);
                    }
                    Marshal.FreeHGlobal(ptrHubName);
                    CloseHandle(h);
                }
                Paths.Add(Path);
            }
            return Paths;
        }

        List<(string, List<(int, USB_DEVICE_DESCRIPTOR)>)> GetPorts(List<(string Path, int PortCount)> HubPaths)
        {
            var Ports = new List<(string Path, List<(int PortNumber, USB_DEVICE_DESCRIPTOR Descriptor)> Device)>();
            foreach (var (Path, PortCount) in HubPaths)
            {
                var PortDescriptorPair = new List<(int PortNumber, USB_DEVICE_DESCRIPTOR Descriptor)>();
                (string Path, List<(int PortNumber, USB_DEVICE_DESCRIPTOR Descriptor)> Devices) Port = (Path, PortDescriptorPair);

                IntPtr h = CreateFile(Path, GENERIC_WRITE, FILE_SHARE_WRITE, IntPtr.Zero, OPEN_EXISTING, 0, IntPtr.Zero);
                if (INVALID_HANDLE_VALUE != h.ToInt64())
                {
                    int nBytes = Marshal.SizeOf(typeof(USB_NODE_CONNECTION_INFORMATION_EX));
                    IntPtr ptrNodeConnection = Marshal.AllocHGlobal(nBytes);

                    // loop thru all of the ports on the hub. BTW: Ports are numbered starting at 1
                    for (int i = 1; i <= PortCount; i++)
                    {
                        USB_NODE_CONNECTION_INFORMATION_EX NodeConnection = new USB_NODE_CONNECTION_INFORMATION_EX();
                        NodeConnection.ConnectionIndex = i;
                        Marshal.StructureToPtr(NodeConnection, ptrNodeConnection, true);

                        if (DeviceIoControl(h, IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX, ptrNodeConnection, nBytes, ptrNodeConnection, nBytes, out int nBytesReturned, IntPtr.Zero))
                        {
                            NodeConnection = (USB_NODE_CONNECTION_INFORMATION_EX)Marshal.PtrToStructure(ptrNodeConnection, typeof(USB_NODE_CONNECTION_INFORMATION_EX));
                            if (NodeConnection.ConnectionStatus != (int)USB_CONNECTION_STATUS.DeviceConnected) continue;
                            if (Convert.ToBoolean(NodeConnection.DeviceIsHub)) continue;

                            //Following combination is for Multi-interface devices, video function might be there
                            byte DeviceClass = NodeConnection.DeviceDescriptor.bDeviceClass;
                            byte DeviceSubClass = NodeConnection.DeviceDescriptor.bDeviceSubClass;
                            byte DeviceProtocol = NodeConnection.DeviceDescriptor.bDeviceProtocol;
                            if (0xEF == DeviceClass && 0x02 == DeviceSubClass && 0x01 == DeviceProtocol)
                            {
                                var ExistingPort = Ports.SingleOrDefault(s => s.Path == Path);
                                if ((null, null) == ExistingPort)
                                {
                                    Port.Devices.Add((i, NodeConnection.DeviceDescriptor));
                                    Ports.Add(Port);
                                }
                                else ExistingPort.Device.Add((i, NodeConnection.DeviceDescriptor));
                            }
                        }
                    }
                    Marshal.FreeHGlobal(ptrNodeConnection);
                    CloseHandle(h);
                }
            }
            return Ports;
        }

        List<USBDevice> GetVideoDevices(List<(string Path, List<(int PortNumber, USB_DEVICE_DESCRIPTOR Descriptor)> Devices)> Ports)
        {
            List<USBDevice> USBDevices = new List<USBDevice>();
            foreach (var (Path, Devices) in Ports)
            {
                foreach (var (PortNumber, Descriptor) in Devices)
                {
                    for (byte ConfigNumber = 0; ConfigNumber < Descriptor.bNumConfigurations; ConfigNumber++)
                    {
                        string NullString = new string((char)0, BUFFER_SIZE / Marshal.SystemDefaultCharSize);
                        IntPtr h = CreateFile(Path, GENERIC_WRITE, FILE_SHARE_WRITE, IntPtr.Zero, OPEN_EXISTING, 0, IntPtr.Zero);
                        if (INVALID_HANDLE_VALUE != h.ToInt64())
                        {
                            USB_DESCRIPTOR_REQUEST Request = new USB_DESCRIPTOR_REQUEST();
                            Request.ConnectionIndex = PortNumber;
                            Request.SetupPacket.wValue = (short)((USB_CONFIGURATION_DESCRIPTOR_TYPE << 8) + ConfigNumber);

                            IntPtr ptrRequest = Marshal.StringToHGlobalAuto(NullString);
                            Marshal.StructureToPtr(Request, ptrRequest, true);

                            int nBytes = BUFFER_SIZE;
                            if (DeviceIoControl(h, IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION, ptrRequest, nBytes, ptrRequest, nBytes, out int nBytesReturned, IntPtr.Zero))
                            {
                                IntPtr ptrFullDescriptor = new IntPtr(ptrRequest.ToInt32() + Marshal.SizeOf(Request));

                                int BufferSize = nBytesReturned - Marshal.SizeOf(Request);
                                byte[] FullDescriptorBuffer = new byte[BufferSize];
                                for (int i = 0; i < BufferSize; i++)
                                    FullDescriptorBuffer[i] = Marshal.ReadByte(ptrFullDescriptor, i);

                                if(HasVideoStreamingFunction(FullDescriptorBuffer, BufferSize))
                                {
                                    USBDevices.Add(new USBDevice {
                                        ConfigNumber = ConfigNumber,
                                        ConfigDescriptors = FullDescriptorBuffer,
                                        Product = GetProductName(h, PortNumber, Descriptor.iProduct)
                                    });
                                }
                            }
                            Marshal.FreeHGlobal(ptrRequest);
                        }
                        CloseHandle(h);
                    }
                }
            }
            return USBDevices;
        }

        bool HasVideoStreamingFunction(byte[] Buffer, int Size)
        {
            if (3 > Size) return false;
            int Index = 0;
            do
            {
                if (0x04 == Buffer[Index + 1])          //Is it standard interface descriptor?
                    if (0x0E == Buffer[Index + 5])      //Is interfaces class CC_VIDEO?
                        if (0x02 == Buffer[Index + 6])  //Is interface sub-class SC_VIDEOSCREAMING?
                            return true;

                Index += Buffer[Index];
            }
            while (Index < Size);
            return false;
        }

        string GetProductName(IntPtr h, int PortNumber, byte iProduct)
        {
            string ProductName = "";
            int nBytes = BUFFER_SIZE;
            string NullString = new string((char)0, BUFFER_SIZE / Marshal.SystemDefaultCharSize);

            // build a request for string descriptor
            USB_DESCRIPTOR_REQUEST Request = new USB_DESCRIPTOR_REQUEST();
            Request.ConnectionIndex = PortNumber;
            Request.SetupPacket.wValue = (short)((USB_STRING_DESCRIPTOR_TYPE << 8) + iProduct);
            Request.SetupPacket.wLength = (short)(nBytes - Marshal.SizeOf(Request));
            Request.SetupPacket.wIndex = 0x409; // Language Code
                                                // Geez, I wish C# had a Marshal.MemSet() method
            IntPtr ptrRequest = Marshal.StringToHGlobalAuto(NullString);
            Marshal.StructureToPtr(Request, ptrRequest, true);

            // Use an IOCTL call to request the String Descriptor
            if (DeviceIoControl(h, IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION, ptrRequest, nBytes, ptrRequest, nBytes, out int nBytesReturned, IntPtr.Zero))
            {
                // the location of the string descriptor is immediately after the Request structure
                IntPtr ptrStringDesc = new IntPtr(ptrRequest.ToInt32() + Marshal.SizeOf(Request));
                USB_STRING_DESCRIPTOR StringDesc = (USB_STRING_DESCRIPTOR)Marshal.PtrToStructure(ptrStringDesc, typeof(USB_STRING_DESCRIPTOR));
                ProductName = StringDesc.bString;
            }
            Marshal.FreeHGlobal(ptrRequest);
            return string.IsNullOrWhiteSpace(ProductName) ? "--noname--" : ProductName;
        }
    }
}
