﻿using System;
using System.Runtime.InteropServices;

namespace ReadingUSBCameraDescriptors.USB
{
    partial class USBHost
    {
        //-----------------------------------------------------------------------------------------
        #region Enums

        //typedef enum _USB_HUB_NODE {
        //    UsbHub,
        //    UsbMIParent
        //} USB_HUB_NODE;
        protected enum USB_HUB_NODE
        {
            UsbHub,
            UsbMIParent
        }

        //typedef enum _USB_CONNECTION_STATUS {
        //    NoDeviceConnected,
        //    DeviceConnected,
        //    DeviceFailedEnumeration,
        //    DeviceGeneralFailure,
        //    DeviceCausedOvercurrent,
        //    DeviceNotEnoughPower,
        //    DeviceNotEnoughBandwidth,
        //    DeviceHubNestedTooDeeply,
        //    DeviceInLegacyHub
        //} USB_CONNECTION_STATUS, *PUSB_CONNECTION_STATUS;
        protected enum USB_CONNECTION_STATUS
        {
            NoDeviceConnected,
            DeviceConnected,
            DeviceFailedEnumeration,
            DeviceGeneralFailure,
            DeviceCausedOvercurrent,
            DeviceNotEnoughPower,
            DeviceNotEnoughBandwidth,
            DeviceHubNestedTooDeeply,
            DeviceInLegacyHub
        }

        #endregion
        //-----------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------
        #region Structs

        //typedef struct _USB_ROOT_HUB_NAME {
        //    ULONG  ActualLength;
        //    WCHAR  RootHubName[1];
        //} USB_ROOT_HUB_NAME, *PUSB_ROOT_HUB_NAME;
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        struct USB_ROOT_HUB_NAME
        {
            public int ActualLength;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = BUFFER_SIZE)]
            public string RootHubName;
        }

        //typedef struct _SP_DEVICE_INTERFACE_DATA {
        //  DWORD cbSize;
        //  GUID InterfaceClassGuid;
        //  DWORD Flags;
        //  ULONG_PTR Reserved;
        //} SP_DEVICE_INTERFACE_DATA,  *PSP_DEVICE_INTERFACE_DATA;
        struct SP_DEVICE_INTERFACE_DATA
        {
            public int cbSize;
            public Guid InterfaceClassGuid;
            public int Flags;
            public IntPtr Reserved;
        }

        //typedef struct _SP_DEVINFO_DATA {
        //  DWORD cbSize;
        //  GUID ClassGuid;
        //  DWORD DevInst;
        //  ULONG_PTR Reserved;
        //} SP_DEVINFO_DATA,  *PSP_DEVINFO_DATA;
        struct SP_DEVINFO_DATA
        {
            public int cbSize;
            public Guid ClassGuid;
            public int DevInst;
            public IntPtr Reserved;
        }

        //typedef struct _SP_DEVICE_INTERFACE_DETAIL_DATA {
        //  DWORD cbSize;
        //  TCHAR DevicePath[ANYSIZE_ARRAY];
        //} SP_DEVICE_INTERFACE_DETAIL_DATA,  *PSP_DEVICE_INTERFACE_DETAIL_DATA;
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        struct SP_DEVICE_INTERFACE_DETAIL_DATA
        {
            public int cbSize;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = BUFFER_SIZE)]
            public string DevicePath;
        }

        //typedef struct _USB_NODE_CONNECTION_INFORMATION_EX {
        //    ULONG  ConnectionIndex;
        //    USB_DEVICE_DESCRIPTOR  DeviceDescriptor;
        //    UCHAR  CurrentConfigurationValue;
        //    UCHAR  Speed;
        //    BOOLEAN  DeviceIsHub;
        //    USHORT  DeviceAddress;
        //    ULONG  NumberOfOpenPipes;
        //    USB_CONNECTION_STATUS  ConnectionStatus;
        //    USB_PIPE_INFO  PipeList[0];
        //} USB_NODE_CONNECTION_INFORMATION_EX, *PUSB_NODE_CONNECTION_INFORMATION_EX;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct USB_NODE_CONNECTION_INFORMATION_EX
        {
            public int ConnectionIndex;
            public USB_DEVICE_DESCRIPTOR DeviceDescriptor;
            public byte CurrentConfigurationValue;
            public byte Speed;
            public byte DeviceIsHub;
            public short DeviceAddress;
            public int NumberOfOpenPipes;
            public int ConnectionStatus;
        }

        //typedef struct _USB_HUB_DESCRIPTOR {
        //    UCHAR  bDescriptorLength;
        //    UCHAR  bDescriptorType;
        //    UCHAR  bNumberOfPorts;
        //    USHORT  wHubCharacteristics;
        //    UCHAR  bPowerOnToPowerGood;
        //    UCHAR  bHubControlCurrent;
        //    UCHAR  bRemoveAndPowerMask[64];
        //} USB_HUB_DESCRIPTOR, *PUSB_HUB_DESCRIPTOR;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct USB_HUB_DESCRIPTOR
        {
            public byte bDescriptorLength;
            public byte bDescriptorType;
            public byte bNumberOfPorts;
            public short wHubCharacteristics;
            public byte bPowerOnToPowerGood;
            public byte bHubControlCurrent;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
            public byte[] bRemoveAndPowerMask;
        }

        //typedef struct _USB_HUB_INFORMATION {
        //    USB_HUB_DESCRIPTOR HubDescriptor;
        //    BOOLEAN HubIsBusPowered;
        //} USB_HUB_INFORMATION, *PUSB_HUB_INFORMATION;
        [StructLayout(LayoutKind.Sequential)]
        struct USB_HUB_INFORMATION
        {
            public USB_HUB_DESCRIPTOR HubDescriptor;
            public byte HubIsBusPowered;
        }

        //typedef struct _USB_NODE_INFORMATION {
        //    USB_HUB_NODE  NodeType;
        //    union {
        //        USB_HUB_INFORMATION  HubInformation;
        //        USB_MI_PARENT_INFORMATION  MiParentInformation;
        //    } u;
        //} USB_NODE_INFORMATION, *PUSB_NODE_INFORMATION;
        [StructLayout(LayoutKind.Sequential)]
        struct USB_NODE_INFORMATION
        {
            public int NodeType;
            public USB_HUB_INFORMATION HubInformation;          // Yeah, I'm assuming we'll just use the first form
        }

        //typedef struct _USB_DESCRIPTOR_REQUEST {
        //  ULONG ConnectionIndex;
        //  struct {
        //    UCHAR  bmRequest;
        //    UCHAR  bRequest;
        //    USHORT  wValue;
        //    USHORT  wIndex;
        //    USHORT  wLength;
        //  } SetupPacket;
        //  UCHAR  Data[0];
        //} USB_DESCRIPTOR_REQUEST, *PUSB_DESCRIPTOR_REQUEST
        [StructLayout(LayoutKind.Sequential)]
        struct USB_SETUP_PACKET
        {
            public byte bmRequest;
            public byte bRequest;
            public short wValue;
            public short wIndex;
            public short wLength;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct USB_DESCRIPTOR_REQUEST
        {
            public int ConnectionIndex;
            public USB_SETUP_PACKET SetupPacket;
            //public byte[] Data;
        }

        //typedef struct _USB_DEVICE_DESCRIPTOR {
        //    UCHAR  bLength;
        //    UCHAR  bDescriptorType;
        //    USHORT  bcdUSB;
        //    UCHAR  bDeviceClass;
        //    UCHAR  bDeviceSubClass;
        //    UCHAR  bDeviceProtocol;
        //    UCHAR  bMaxPacketSize0;
        //    USHORT  idVendor;
        //    USHORT  idProduct;
        //    USHORT  bcdDevice;
        //    UCHAR  iManufacturer;
        //    UCHAR  iProduct;
        //    UCHAR  iSerialNumber;
        //    UCHAR  bNumConfigurations;
        //} USB_DEVICE_DESCRIPTOR, *PUSB_DEVICE_DESCRIPTOR ;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct USB_DEVICE_DESCRIPTOR
        {
            public byte bLength;
            public byte bDescriptorType;
            public short bcdUSB;
            public byte bDeviceClass;
            public byte bDeviceSubClass;
            public byte bDeviceProtocol;
            public byte bMaxPacketSize0;
            public short idVendor;
            public short idProduct;
            public short bcdDevice;
            public byte iManufacturer;
            public byte iProduct;
            public byte iSerialNumber;
            public byte bNumConfigurations;
        }

        //typedef struct _USB_STRING_DESCRIPTOR {
        //    UCHAR bLength;
        //    UCHAR bDescriptorType;
        //    WCHAR bString[1];
        //} USB_STRING_DESCRIPTOR, *PUSB_STRING_DESCRIPTOR;
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        struct USB_STRING_DESCRIPTOR
        {
            public byte bLength;
            public byte bDescriptorType;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 255)]
            public string bString;
        }

        #endregion
        //-----------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------
        #region Functions

        //HANDLE CreateFile(
        //  LPCTSTR lpFileName,
        //  DWORD dwDesiredAccess,
        //  DWORD dwShareMode,
        //  LPSECURITY_ATTRIBUTES lpSecurityAttributes,
        //  DWORD dwCreationDisposition,
        //  DWORD dwFlagsAndAttributes,
        //  HANDLE hTemplateFile
        //);
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern IntPtr CreateFile(
           string lpFileName,
           int dwDesiredAccess,
           int dwShareMode,
           IntPtr lpSecurityAttributes,
           int dwCreationDisposition,
           int dwFlagsAndAttributes,
           IntPtr hTemplateFile
        );

        //BOOL CloseHandle(HANDLE hObject);
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern bool CloseHandle(IntPtr hObject);

        //BOOL DeviceIoControl(
        //  HANDLE hDevice,
        //  DWORD dwIoControlCode,
        //  LPVOID lpInBuffer,
        //  DWORD nInBufferSize,
        //  LPVOID lpOutBuffer,
        //  DWORD nOutBufferSize,
        //  LPDWORD lpBytesReturned,
        //  LPOVERLAPPED lpOverlapped
        //);
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern bool DeviceIoControl(
            IntPtr hDevice,
            int dwIoControlCode,
            IntPtr lpInBuffer,
            int nInBufferSize,
            IntPtr lpOutBuffer,
            int nOutBufferSize,
            out int lpBytesReturned,
            IntPtr lpOverlapped
        );

        //HDEVINFO SetupDiGetClassDevs(
        //  const GUID* ClassGuid,
        //  PCTSTR Enumerator,
        //  HWND hwndParent,
        //  DWORD Flags
        //);
        [DllImport("setupapi.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SetupDiGetClassDevs(
            ref Guid ClassGuid,
            int Enumerator,
            IntPtr hwndParent,
            int Flags
        );

        //BOOL SetupDiEnumDeviceInterfaces(
        //  HDEVINFO DeviceInfoSet,
        //  PSP_DEVINFO_DATA DeviceInfoData,
        //  const GUID* InterfaceClassGuid,
        //  DWORD MemberIndex,
        //  PSP_DEVICE_INTERFACE_DATA DeviceInterfaceData
        //);
        [DllImport("setupapi.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern bool SetupDiEnumDeviceInterfaces(
            IntPtr DeviceInfoSet,
            IntPtr DeviceInfoData,
            ref Guid InterfaceClassGuid,
            int MemberIndex,
            ref SP_DEVICE_INTERFACE_DATA DeviceInterfaceData
        );

        //BOOL SetupDiGetDeviceInterfaceDetail(
        //  HDEVINFO DeviceInfoSet,
        //  PSP_DEVICE_INTERFACE_DATA DeviceInterfaceData,
        //  PSP_DEVICE_INTERFACE_DETAIL_DATA DeviceInterfaceDetailData,
        //  DWORD DeviceInterfaceDetailDataSize,
        //  PDWORD RequiredSize,
        //  PSP_DEVINFO_DATA DeviceInfoData
        //);
        [DllImport("setupapi.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern bool SetupDiGetDeviceInterfaceDetail(
            IntPtr DeviceInfoSet,
            ref SP_DEVICE_INTERFACE_DATA DeviceInterfaceData,
            ref SP_DEVICE_INTERFACE_DETAIL_DATA DeviceInterfaceDetailData,
            int DeviceInterfaceDetailDataSize,
            ref int RequiredSize,
            ref SP_DEVINFO_DATA DeviceInfoData
        );

        #endregion
        //-----------------------------------------------------------------------------------------
    }
}