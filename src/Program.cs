﻿using ReadingUSBCameraDescriptors.USB;
using System;

namespace ReadingUSBCameraDescriptors
{
    class Program
    {
        static void Main()
        {
            USBHost Host = new USBHost();
            foreach (USBDevice d in Host.GetVideoDevices())
            {
                Console.WriteLine($"Config: {d.ConfigNumber}, Product: {d.Product}");
                d.SaveHtml();
            }
            Console.WriteLine();
            Console.Write("Press ENTER...");
            Console.ReadLine();
        }
    }
}
